package day12;

import static org.openqa.selenium.support.locators.RelativeLocator.with;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Demo2 {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new ChromeDriver();
		driver.get("file:///D:/Sample7.html");
		Thread.sleep(1000);
		
		WebElement java = driver.findElement(By.xpath("//td[text()='Java']"));
		
		driver.findElement(with(By.tagName("input")).toLeftOf(java)).click();
		
		String cost = driver.findElement(with(By.tagName("td")).toRightOf(java)).getText();
		System.out.println(cost);
		String heading = driver.findElement(with(By.tagName("th")).above(java)).getText();
		System.out.println(heading);
		String nextSubheading = driver.findElement(with(By.tagName("td")).below(java)).getText();
		System.out.println(nextSubheading);

	}

}
