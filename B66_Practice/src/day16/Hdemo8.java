package day16;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Hdemo8 {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.amazon.in/");
		Thread.sleep(1000);
		driver.findElement(By.id("twotabsearchtextbox")).sendKeys("iphone");
		Thread.sleep(1000);
		List<WebElement> allASE = driver.findElements(By.xpath("//div[contains(text(),'iphone')]"));

		int count = allASE.size();

		System.out.println(count);

		for (int i = 0; i < count; i++) {
			String text = allASE.get(i).getText();
			System.out.println(text);
		}
		Thread.sleep(1000);
		String expected = "iphone 13";
		for (int i = 0; i < count; i++) {
			String text = allASE.get(i).getText();

			if (expected.equals(text)) {
				allASE.get(i).click();
				break;
			}
		}
	}

}
