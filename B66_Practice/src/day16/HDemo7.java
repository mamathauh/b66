package day16;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HDemo7 {
//print the content of the web table
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new ChromeDriver();
		driver.get("file:///C:/Users/Mamatha/Downloads/Sample5.html");

		List<WebElement> allCells = driver.findElements(By.xpath("//th|//td"));
		List<WebElement> elements = driver.findElements(By.xpath("//tr"));

		for (WebElement cell : allCells) {
			System.out.print(cell.getText() + " ");

		}
		System.out.println("-------------------");
		for (WebElement element : elements) {
			System.out.println(element.getText() + " ");
		}

	}

}
