package day16;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Demo5 {
	public static void main(String[] args) {
		WebDriver driver = new ChromeDriver();
		driver.get("http://www.google.com");
		List<WebElement> e1 = driver.findElements(By.tagName("a"));
		int count = e1.size();
		System.out.println("count " + count);

		for (int i = 0; i < count; i++) {
			WebElement element = e1.get(i);
			String text = element.getText();
			boolean displayed = element.isDisplayed();
			System.out.print(i + " ");
			System.out.print(displayed + " ");
			System.out.println(text + " ");

		}

	}

}
