package day16;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Demo4 {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new ChromeDriver();
		driver.get("file:///D:/Sample13.html");
		Thread.sleep(1000);
		List<WebElement> e1 = driver.findElements(By.xpath("//input[@type='text']"));
		int count = e1.size();
		System.out.println("count " + count);
		// WebElement v = e1.get(0);
		// v.sendKeys("a");
		for (int i = 0; i < count; i++) {
			WebElement v = e1.get(i);
			v.sendKeys("a");

		}
		for (WebElement v : e1) {
			v.sendKeys("b");

		}
	}
}