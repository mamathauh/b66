package day13;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Demo5 {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new ChromeDriver();

		driver.get("https://www.facebook.com/Meta");

		WebElement abt = driver.findElement(By.xpath("//h1[text()='Meta']"));

		File img = abt.getScreenshotAs(OutputType.FILE);
		String path = img.getAbsolutePath();
		System.out.println(path);
		Thread.sleep(20000);
		driver.close();

	}

}
