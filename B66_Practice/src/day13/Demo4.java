package day13;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Demo4 {
	public static void main(String[] args) {
		WebDriver driver = new ChromeDriver();
		driver.get(
				"https://www.facebook.com/campaign/landing.php?campaign_id=14884913640&extra_1=s%7Cc%7C589460569891%7Cb%7Cfacebook%20signin%7C&placement=&creative=589460569891&keyword=facebook%20signin&partner_id=googlesem&extra_2=campaignid%3D14884913640%26adgroupid%3D128696221832%26matchtype%3Db%26network%3Dg%26source%3Dnotmobile%26search_or_content%3Ds%26device%3Dc%26devicemodel%3D%26adposition%3D%26target%3D%26targetid%3Dkwd-3821998899%26loc_physical_ms%3D9061998%26loc_interest_ms%3D%26feeditemid%3D%26param1%3D%26param2%3D&gclid=Cj0KCQiAz9ieBhCIARIsACB0oGIobCI9IKj5x9aYt9MK90yfYwPGD3j3Zs5qfWXjk-aZbHyeuwsOdk8aAnFREALw_wcB");
		WebElement fstName = driver.findElement(By.name("firstname"));

		WebElement lstName = driver.findElement(By.name("lastname"));

		int x1 = fstName.getRect().getX();

		int x2 = lstName.getRect().getX();

		if (x2 > x1) {
			System.out.println("x2 is next to the x1");
		} else {
			System.out.println("x2 is not next to the x1");
		}

	}

}
