package day13;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Demo3 {
	public static void main(String[] args) throws InterruptedException {

		WebDriver driver = new ChromeDriver();
		driver.get("file:///C:/Users/Mamatha/Downloads/Sample9.html");
		Thread.sleep(1000);
		WebElement element = driver.findElement(By.id("A1"));
		String tag = element.getTagName();
		System.out.println(tag);
		String ata = element.getAttribute("type");
		Thread.sleep(1000);
		System.out.println(ata);
		System.out.println("***********");
		boolean v = driver.findElement(By.id("A1")).isDisplayed();

		System.out.println(v);
		v = driver.findElement(By.id("A3")).isDisplayed();

		System.out.println(v);
		v = driver.findElement(By.id("A5")).isSelected();
		System.out.println(v);
		Thread.sleep(1000);
		v = driver.findElement(By.id("A6")).isSelected();
		System.out.println(v);
		v = driver.findElement(By.id("A4")).isEnabled();
		System.out.println(v);
		v = driver.findElement(By.id("A1")).isEnabled();
		System.out.println(v);
		
	}

}
