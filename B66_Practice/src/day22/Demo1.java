package day22;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

class LoginPage {
	private WebElement unTB;
	private WebElement psTB;
	private WebElement login;

	LoginPage(WebDriver driver) {
		unTB = driver.findElement(By.id("username"));
		psTB = driver.findElement(By.name("pwd"));
		login = driver.findElement(By.id("loginButton"));
	}

	void setUsername(String un) {
		unTB.sendKeys(un);
	}

	void setPassword(String pw) {
		psTB.sendKeys(pw);
	}

	void login() {
		login.click();
	}
}

public class Demo1 {
	public static void main(String[] args) {

		WebDriver driver = new ChromeDriver();
		driver.get("https://demo.actitime.com/login.do");

		LoginPage page = new LoginPage(driver);

		page.setUsername("admin");
		page.setPassword("manager");
		page.login();

	}
}