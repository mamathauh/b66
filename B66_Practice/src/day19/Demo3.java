package day19;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Demo3 {
	public static void main(String[] args) {
		WebDriver driver = new ChromeDriver();
		driver.get("file:///D:/Sample16.html");
		WebElement listbox = driver.findElement(By.id("A3"));
		Select select = new Select(listbox);
		String element = select.getWrappedElement().getText();
		System.out.println(element);

		List<WebElement> option = select.getAllSelectedOptions();
		System.out.println(option);

		for (WebElement options : option) {
			System.out.println(options.getText());
		}
	}

}
