package day19;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class PDemo2 {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

		driver.get("file:///D:/Sample16.html");

		WebElement listbox = driver.findElement(By.id("A2"));
		Select select = new Select(listbox);
		select.isMultiple();// true
		Thread.sleep(1000);
		select.selectByIndex(0);// Breakfast

		select.deselectAll();// here there is no select but we wont get the exception it keep quite

		List<WebElement> allOptions = select.getOptions();
		String v = allOptions.get(0).getText();
		System.out.println(v);// Breakfast

		
		select.selectByValue("c");// Snacks

		select.selectByVisibleText("Dinner");

	}

}
