package day14;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Demo8 {
	public static void main(String[] args) throws IOException, InterruptedException {
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.actimind.com/");
		TakesScreenshot t = (TakesScreenshot) driver;

		JavascriptExecutor j = (JavascriptExecutor) driver;

		for (int i = 1; i <= 5; i++) {

			File srcimage = t.getScreenshotAs(OutputType.FILE);
			File destimage = new File("./img/actimind" + i + "png");
			FileUtils.copyFile(srcimage, destimage);
			j.executeScript("window.scrollBy(0,500)");
			Thread.sleep(1000);

		}

	}

}
