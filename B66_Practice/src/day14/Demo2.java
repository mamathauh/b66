package day14;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Demo2 {
	public static void main(String[] args) throws IOException {
		WebDriver driver= new ChromeDriver();
		driver.get("https://mail.google.com/mail/u/0/#inbox");
		TakesScreenshot t= (TakesScreenshot) driver;
		File srcImage = t.getScreenshotAs(OutputType.FILE);
		File destImage= new File("./img/mail.png");
		FileUtils.copyFile(srcImage, destImage);
	}

}
