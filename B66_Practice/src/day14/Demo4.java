package day14;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Demo4 {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.actimind.com/");
		JavascriptExecutor j = (JavascriptExecutor) driver;
		// scroll up
		for (int i = 0; i <= 5; i++) {
			j.executeScript("window.scrollBy(0,500)");
			Thread.sleep(1000);
		}
		// scroll down
		for (int i = 5; i >= 1; i--) {
			j.executeScript("window.scrollBy(0,-500)");
			Thread.sleep(1000);
		}
	}
}
