package day14;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Demo1 {
	public static void main(String[] args) throws IOException {
		WebDriver driver = new ChromeDriver();
		driver.get("https://demo.actitime.com/login.do");
		TakesScreenshot t = (TakesScreenshot) driver;
		File srcImange = t.getScreenshotAs(OutputType.FILE);
		File destImange = new File("./img/actitime.png");
		FileUtils.copyFile(srcImange, destImange);
		driver.quit();
	}

}
