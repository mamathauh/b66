package day21;

import java.time.Duration;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PDemo2 {
	public static void main(String[] args) {
		WebDriver driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

		driver.get("file:///D:/Sample17.html");

		driver.findElement(By.id("A5")).click();
		Set<String> allWH = driver.getWindowHandles();
		for (String w : allWH) {
			driver.switchTo().window(w);
			System.out.println(driver.getTitle());

			driver.close();
		}

	}

}
