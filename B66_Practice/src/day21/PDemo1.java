package day21;

import java.time.Duration;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PDemo1 {
	public static void main(String[] args) {
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		String WH = driver.getWindowHandle();
		System.out.println(WH);
		driver.get("file:///D:/Sample17.html");
		driver.findElement(By.id("A5")).click();

		Set<String> allWH = driver.getWindowHandles();
		int count = allWH.size();
		System.out.println(count);
		for (String w : allWH) {
			System.out.println(w);
		}
		driver.quit();
	}

}
