package day21;

import java.time.Duration;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PDemo4 {
	public static void main(String[] args) {
		WebDriver driver = new ChromeDriver();
		String parent = driver.getWindowHandle();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.get("file:///D:/Sample17.html");
		driver.findElement(By.id("A5")).click();
		Set<String> allWH = driver.getWindowHandles();
		
		allWH.remove(parent);
		int count = allWH.size();
		System.out.println(count);
		for(String w:allWH) {
			driver.switchTo().window(w);
			String actualTitle = driver.getTitle();
			System.out.println(actualTitle);
			
			driver.findElement(By.xpath("//input[@type='text']")).sendKeys(actualTitle);
			
		}
		

	}
}
