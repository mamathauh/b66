package day21;

import java.time.Duration;
import java.util.Scanner;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PDemo3 {
	// closig the particular browser
	public static void main(String[] args) {
		System.out.println("please enter the title");
		Scanner sc = new Scanner(System.in);
		String expectedresult = sc.next();

		String msg = "Browser is not found";
		WebDriver driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.get("file:///D:/Sample17.html");
		driver.findElement(By.id("A5")).click();
		Set<String> allWh = driver.getWindowHandles();
		for (String w : allWh) {
			driver.switchTo().window(w);
			String actualResult = driver.getTitle();

			if (actualResult.equalsIgnoreCase(expectedresult)) {
				msg = "Browser is foud and closing that particular browser";
				driver.close();

			}

		}
		System.out.println(msg);
	}
}
