package day21;

import java.time.Duration;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PDemo7 {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
		driver.get("https://demo.actitime.com/login.do");
		driver.findElement(By.linkText("actiTIME Inc.")).click();
		Thread.sleep(1000);

		Set<String> allWH = driver.getWindowHandles();

		for (String w : allWH) {
			driver.switchTo().window(w);
			System.out.println(driver.getTitle());
			System.out.println(w);
			
		}
		driver.findElement(By.linkText("Get started")).click();

	}
}
