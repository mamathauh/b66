package day21;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;

public class PDemo8 {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
		driver.get("https://www.google.com/");
		Thread.sleep(1000);
		driver.switchTo().newWindow(WindowType.TAB);
		driver.get("https://fb.com");
		
		driver.quit();
		driver = new ChromeDriver();
		driver.get("https://www.google.com/");
		Thread.sleep(1000);
		driver.switchTo().newWindow(WindowType.WINDOW);
		Thread.sleep(1000);
		driver.get("https://fb.com");
		driver.close();
		}

}
