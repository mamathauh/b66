package day5;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Demo4 {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new ChromeDriver();

		driver.get("http:\\www.google.com");
		Thread.sleep(500);
		driver.manage().window().maximize();
		Thread.sleep(500);
		driver.manage().window().fullscreen();
		Thread.sleep(500);
		driver.manage().window().minimize();
		driver.quit();
	}

}
