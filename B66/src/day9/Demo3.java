package day9;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Demo3 {

	public static void main(String[] args) throws InterruptedException {

		WebDriver driver = new ChromeDriver();

		driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
		Thread.sleep(5000);

		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("Admin");

		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("admin123");

		driver.findElement(By.xpath("//button[@type='submit']")).click();

		Thread.sleep(5000);
		
		String actualtitle = driver.getTitle();
		System.out.println("Actual title is"+actualtitle);
		
		String expectedresult="Actual title isOrangeHRM";
		System.out.println("Expected result is "+expectedresult);
		
		if(actualtitle.equals(expectedresult)) {

			System.out.println("Pass: Home page is displayed");
		}
		else
		{
			System.out.println("FAIL: Home page is not displayed");
		}
		
	
	String actualURL=driver.getCurrentUrl();
	System.out.println("Actual URL is:"+actualURL);
	
	String expectedURL="index";
	System.out.println("Expected URL contains:"+expectedURL);
	
	if(actualURL.contains(expectedURL))
	{
		System.out.println("Pass: Home page is displayed");
	}
	else
	{
		System.out.println("FAIL: Home page is not displayed");
	}
}
}