package day24;

import org.testng.Reporter;
import org.testng.annotations.Test;

public class Demo4 {

	@Test
	void testA()
	{
		Reporter.log("1.Executing TestA of Demo4");//only in HTML
		Reporter.log("2.Executing TestA of Demo4",false);//only in HTML
		
		Reporter.log("2.Executing TestA of Demo4",true);//HTML & console
		System.out.println("3.Executing TestA of Demo4");//only in console
	}
}
