package day14;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//scroll to specific element
public class Demo6 {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver=new ChromeDriver();
		driver.get("https://www.actimind.com");
		driver.manage().window().maximize();
		Thread.sleep(1000);
		WebElement ourClient = driver.findElement(By.xpath("//h3[contains(text(),'Our client')]"));
		int y=ourClient.getLocation().getY();
		System.out.println(y);
	
		JavascriptExecutor j=(JavascriptExecutor) driver;
		j.executeScript("window.scrollTo(0,"+y+")");
		//driver.quit();
	}

}
