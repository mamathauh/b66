package day14;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
//scrolling
public class Demo8 {
	public static void main(String[] args) throws Exception {
		WebDriver driver=new ChromeDriver();
		driver.get("https://www.actimind.com");
		JavascriptExecutor j=(JavascriptExecutor) driver;
		TakesScreenshot t=(TakesScreenshot) driver;
		
		for(int i=1;i<=5;i++)
		{
			File scrImgFile = t.getScreenshotAs(OutputType.FILE);
			File dstImgFile = new File("./image/mind"+i+".png");
			FileUtils.copyFile(scrImgFile, dstImgFile);
			j.executeScript("window.scrollBy(0,500)");
			Thread.sleep(1000);
		}

		
	}

}
