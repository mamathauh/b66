package day14;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//how do handle disabled element- using JSE
public class Demo7 {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver=new ChromeDriver();
		driver.get("file:///D:/Sample9.html");
		Thread.sleep(1000);
//		driver.findElement(By.id("A4")).sendKeys("Akshara");
		
		String code="document.getElementById('A4').value='Ravi'";//sendKeys
		JavascriptExecutor j=(JavascriptExecutor) driver;
		j.executeScript(code);
		
		Thread.sleep(1000);
		String code2="document.getElementById('A4').value=''";//clear
		j.executeScript(code2);
	}

}
