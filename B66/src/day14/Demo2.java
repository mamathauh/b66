package day14;

import java.io.File;

import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//Taking the screenshot of page- it will not scroll and take the screenshot
public class Demo2 {
	public static void main(String[] args) throws InterruptedException, IOException {
		WebDriver driver=new ChromeDriver();
		driver.get("https://news.google.com/");
		Thread.sleep(1000);
		TakesScreenshot t=(TakesScreenshot) driver;
		File scrImgFile = t.getScreenshotAs(OutputType.FILE);
		File dstImgFile = new File("./image/news.png");
		FileUtils.copyFile(scrImgFile, dstImgFile);
		driver.quit();
		
	}
}
