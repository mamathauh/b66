package day14;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Demo3 {
//to run js from selenium use executeScript method of JavascriptExecutor Interface
	public static void main(String[] args) {
		WebDriver driver=new ChromeDriver();
		driver.get("https://www.actimind.com");
		JavascriptExecutor j=(JavascriptExecutor) driver;
		j.executeScript("alert('Hi, i am mamatha')");

	}

}
