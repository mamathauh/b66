package day14;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import ru.yandex.qatools.ashot.shooting.ShootingStrategy;
//scrolling
public class Demo9 {
	public static void main(String[] args) throws Exception {
		WebDriver driver=new ChromeDriver();
		driver.get("https://www.actimind.com");
		AShot a = new AShot();
		
		Screenshot s = a.shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver);
		
		BufferedImage image = s.getImage();
		File dstImageFile=new File("./image/actimind.png");
		ImageIO.write(image, "png", dstImageFile);
		driver.quit();
		
	}

}
