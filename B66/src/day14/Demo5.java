package day14;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
//scrolling
public class Demo5 {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver=new ChromeDriver();
		driver.get("https://www.actimind.com");
		JavascriptExecutor j=(JavascriptExecutor) driver;
		//scroll down

		for(int i=1;i<=5;i++)
		{
			int a=100*i;
			System.out.println(a);
			
			j.executeScript("window.scrollTo(0,"+a+")");
			Thread.sleep(1000);
		}

		for(int i=5;i>=1;i--)
		{
			int a=100*i;
			System.out.println(a);
			
			j.executeScript("window.scrollTo(0,"+a+")");
			Thread.sleep(1000);
		}
	}

}
