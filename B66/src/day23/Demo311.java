package day23;

class Q
{
	int i=0;
	
	int test()
	{
		return i;
	}
}

class P extends Q
{
	int i=10;
}

class C extends P
{
	int i=20;

	C(int i)
	{
		System.out.println(i);
		System.out.println(this.i);
		System.out.println(super.i);
		System.out.println(test());
	}
}
public class Demo311 {

	public static void main(String[] args) {
		C a1=new C(123);

	}

}
