package day13;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.Color;

public class HDemo8 {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new ChromeDriver();
		driver.get("https://demo.actitime.com/login.do");
		driver.manage().window().maximize();
		WebElement unTB = driver.findElement(By.id("username"));
		WebElement pwTB = driver.findElement(By.name("pwd"));
		int y1 = unTB.getRect().getY();
		int y2 = pwTB.getRect().getY();
		if (y2 > y1) {
			System.out.println("Password present in below the username");
		} else {
			System.out.println("Password is not  present in below the username");
		}

	}

}