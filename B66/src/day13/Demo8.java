package day13;

import java.io.File;


import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.Color;

public class Demo8 {
//png-->Portable Network Graphics
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver=new ChromeDriver();
		driver.get("https://demo.actitime.com/login.do");
		driver.manage().window().maximize();
		WebElement unTB = driver.findElement(By.id("username"));
		File img1 = unTB.getScreenshotAs(OutputType.FILE);//to take the screenshot of UNTB
		String path = img1.getAbsolutePath();//temp
		System.out.println(path);
		Thread.sleep(10000);
		driver.quit();
		
	}
}
