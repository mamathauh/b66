package day4;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Demo3 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://www.fb.com");// entering the URL
		String u = driver.getTitle();
		System.out.println(u);// printing the title
		String url = driver.getCurrentUrl();
		System.out.println(url);
	}

}
