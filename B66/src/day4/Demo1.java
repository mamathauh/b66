package day4;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.chrome.ChromeDriver;

public class Demo1 {

	public static void main(String[] args) throws InterruptedException, MalformedURLException {
		// JAVA: create Chrome Object
		// Sel: Open Chrome Browser
		// System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");

		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.navigate().to("http://www.google.com");
		Thread.sleep(1000);

		driver.navigate().back();

		Thread.sleep(1000);

		driver.navigate().to("https://www.selenium.dev/");

		Thread.sleep(1000);

		URL u = new URL("https://chromedriver.chromium.org/");

		driver.navigate().to(u);
		Thread.sleep(1000);

		driver.navigate().forward();
		Thread.sleep(1000);
		driver.navigate().refresh();
		Thread.sleep(1000);
		driver.quit();

	}

}