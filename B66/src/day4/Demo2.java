package day4;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class Demo2 {

	public static void testBrowser(WebDriver driver) throws InterruptedException {
		driver.get("http://www.google.com");// opening the chrome browser and open the google chrome
		String m = driver.getTitle();
		System.out.println(m);
		Thread.sleep(1000);
		driver.close();
	}

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		WebDriver driver1 = new ChromeDriver();
		Demo2.testBrowser(driver1);
		WebDriver driver2 = new EdgeDriver();
		Demo2.testBrowser(driver2);

	}

}
