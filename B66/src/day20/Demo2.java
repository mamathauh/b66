package day20;

import java.io.File;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Demo2 {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.get("file:///D:/Sample17.html");
		Thread.sleep(1000);
		driver.findElement(By.id("A3")).click();
		driver.findElement(By.id("PageLink_9")).click();
		driver.findElement(By.id("DirectLink_13")).click();
		// home work- check file is present or not
		File f = new File("C:\\Users\\Mamatha\\Downloads");
		Thread.sleep(5000);
		boolean FilePresent = f.exists();
		if (FilePresent) {
			System.out.println("File is present ");
		} else {
			System.out.println("File is not present ");
		}

	}

}
